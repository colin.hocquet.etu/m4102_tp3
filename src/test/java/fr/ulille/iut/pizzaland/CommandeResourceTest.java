package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeResourceTest {
	private CommandeDao dao;

	protected Application configure() {
		return new ApiV1();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/commande").request().get();

		// Vérification de la valeur de retour (200)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// Vérification de la valeur retournée (liste vide)
		List<CommandeDto> commande;
		commande = response.readEntity(new GenericType<List<CommandeDto>>(){});

		assertEquals(0, commande.size());
	}
	  @Test
	    public void testGetExistingPizza() {

	        Commande commande = new Commande();
	        commande.setName("4 fromages");
	        Response response = target("/commande").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

	        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

	        Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
	        assertEquals(commande,result);
	    }
}