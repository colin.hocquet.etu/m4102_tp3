package fr.ulille.iut.pizzaland.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;

@Path("/pizza")
public class CommandeResource {

	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
	}

	@GET
	public List<CommandeDto> getAll() {

		return new ArrayList<CommandeDto>();
	}
	

    @GET
  @Path("{id}")
  public CommandeDto getOneCommande(@PathParam("id") UUID id) {
    	Commande commande = new Commande();
    commande.setId(id); // juste pour avoir le même id pour le test
    commande.setName("4 fromages");
	  
	  return Commande.toDto(commande);
  }
}