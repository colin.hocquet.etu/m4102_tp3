# Projet REST avec Jersey

## Récupération du projet initial
Pour récupérer le projet vous pouvez utiliser la commande `git clone
https://gitlab.univ-lille.fr/yvan.peter/m4102_tp3.git`

L'arborescence ci-dessous vous montre le contenu du projet qui vous
servira de point de départ. Maven est configuré grâce au fichier
`pom.xml` qui permet entre autre de spécifier les dépendances du
projet.

La classe `ApiV1` sera le point d'entrée de notre application REST qui
permet de configurer le chemin de l'URI (`@ApplicationPath`) ainsi que
les paquetages Java qui contiennent les ressources.

~~~
.
├── architecture.svg
├── pom.xml
├── README.md
└── src
    ├── main
    │   ├── java
    │   │   └── fr
    │   │       └── ulille
    │   │           └── iut
    │   │               └── pizzaland
    │   │                   ├── ApiV1.java
    │   │                   ├── BDDFactory.java
    │   │                   ├── beans
    │   │                   ├── dao
    │   │                   │   ├── UUIDArgumentFactory.java
    │   │                   │   └── UUIDArgument.java
    │   │                   ├── dto
    │   │                   │   └── IngredientDto.java
    │   │                   ├── Main.java
    │   │                   └── resources
    │   │                       ├── BDDClearRessource.java
    │   │                       └── IngredientResource.java
    │   └── resources
    │       ├── ingredients.json
    │       └── logging.properties
    └── test
        ├── java
        │   └── fr
        │       └── ulille
        │           └── iut
        │               └── pizzaland
        │                   └── IngredientResourceTest.java
        └── resources
            └── logging.properties
~~~
	
## Développement d'une ressource *pizza*

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *pizza*. Celle-ci devrait répondre aux URI suivantes :

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizza             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizza (I2)                                           |
| /pizza/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza (I2) ou 404                                            |
| /pizza/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizza             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (I1) | Nouvel ingrédient (I2)<br>409 si l'ingrédient existe déjà (même nom) |
| /pizza/{id}        | DELETE      |                                                              |                 |                                                                      |


Une pizza comporte une liste d'ingrédient, un id et un nom. Sa
représentation JSON (I2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "4 fromages"
      "ingredient" : "reblochon" , "chèvre" ,"parmesan" , "bleu"
    }

Lors de la création, l'identifiant n'est pas connu car il sera fourni
par le JavaBean qui représente une pizza. Aussi on aura une
représentation JSON (I1) qui comporte uniquement le nom :

    { "name": "4 fromages"
      "ingredient" : "reblochon" , "chèvre" ,"parmesan" , "bleu" }
      
### Architecture logicielle de la solution

La figure ci-dessous présente l'architecture globale qui devra être
mise en place pour notre développement :

![Architecture de la solution](architecture.svg "Architecture")

#### JavaBeans
JavaBean est un standard pour les objets Java permettant de les créer
et de les initialiser et de les manipuler facilement. Pour cela ces
objets doivent respecter un ensemble de conventions :

  - la classe est sérialisable
  - elle fournit au moins un constructeur vide
  - les attributs privés de la classe sont manipulables via des
    méthodes publiques **get**_Attribut_ et **set**_Attribut_

Les DTO et la classe `Pizza`décrits dans la suite sont des
JavaBeans.

#### Data Transfer Object (DTO)
Les DTO correspondent à la représentation des données qui sera
transportée par HTTP. Ce sont des Javabeans qui possèdent les même
propriétés que la représentation (avec les getter/setter
correspondants).

Jersey utilisera les *setter* pour initialiser l'objet à partir
de la représentation JSON ou XML et les *getter* pour créer la
représentation correspondante.

#### Data Access Object (DAO)
Le DAO permet de faire le lien entre la représentation objet et le
contenu de la base de données.

Nous utiliserons la [librairie JDBI](http://jdbi.org/) qui permet
d'associer une interface à des requêtes SQL.
La classe `BDDFactory` qui vous est fournie permet un accès facilité
aux fonctionnalités de JDBI.

#### La représentation des données manipulées par la ressource
La classe `Pizza` est un JavaBean qui représente ce qu'est un
ingrédient. Elle porte des méthodes pour passer de cette
représentation aux DTO.

Cela permet de découpler l'implémentation de la ressource qui traite
les requêtes HTTP et la donnée manipulée.

Cette classe pourrait
porter des comportements liés à cette donnée (par ex. calcul de TVA).

## Mise en œuvre

### Une première implémentation : récupérer les pizzas existantes
Nous allons réaliser un développement dirigé par les tests. Dans un
premier temps, nous allons commencer par un test qui récupère une
liste de pizzas vide qui sera matérialisée par un tableau JSON
vide `[]`.

Le code suivant qui se trouve dans la classe `PizzaResourceTest`
montre la mise en place de l'environnement (`configure()`) et l'amorce
d'un premier test.

~~~java
public class PizzaResourceTest extends JerseyTest {
    private PizzaDao dao;
	  
    @Override
    protected Application configure() {
      return new ApiV1();
    }

    @Test
    public void testGetEmptyList() {
    	 Response response = target("/pizza").request().get();

      // Vérification de la valeur de retour (200)
      assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
  	  
  	// Vérification de la valeur retournée (liste vide)
  	List<PizzaDto> pizza;
  	pizza = response.readEntity(new GenericType<List<PizzaDto>>(){});

      assertEquals(0, pizza.size());
    }
}
~~~

En héritant de JerseyTest, votre classe de test se comporte comme un
[`Client`
JAX-RS](https://docs.oracle.com/javaee/7/api/jakarta/ws/rs/client/Client.html). La
méthode `target()` notamment permet de préparer une requête sur une
URI particulière.


Vous pouvez compiler votre code ainsi que les tests au moyen
des commandes `mvn compile` et `mvn test-compile`. La compilation du
code et des tests se fera automatiquement si nécessaire quand vous
faites un `mvn test`.

Pour pouvoir compiler ce premier test, il faut au minimum fournir le
DTO `IngredientDto`.
Pour commencer,  on se contentera de l'implémentation minimale
suivante :

~~~java
package fr.ulille.iut.pizzaland.dto;

  public class PizzaDto {

  public PizzaDto() {
  }
}
~~~

Pour réussir, ce premier test, nous allons mettre en place la
ressource `IngredientResource`.

Une première implémentation de la ressource pourra avoir la forme
suivante :

~~~java
@Path("/pizza")
public class PizzaResource {

  @Context
  public UriInfo uriInfo;

  public PizzaResource() {
  }

  @GET
  public List<PizzaDto> getAll() {

    return new ArrayList<PizzaDto>();
  }
}
~~~

Avec cette première implémentation, on va pouvoir tester notre
ressource : 

~~~
$ mvn test
	
Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0
~~~

### Récupérer une pizza existant
Nous allons continuer en ajoutant la possibilité de récupérer une
pizza particulière à partir de son identifiant.
Pour cela voici un premier test qui permettra de vérifier cela :

~~~java
	 
  @Test
    public void testGetExistingPizza() {

        Pizza pizza = new Pizza();
        pizza.setName("4 fromages");
        Response response = target("/pizza").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizza,result);
    }
~~~

Vous pourrez vérifier que le test échoue au moyen de la commande `mvn test`

Afin de réussir ce test, nous devons compléter la classe PizzaDto
avec les getter/setter correspondant aux propriétés de l'object JSON.

~~~java
package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

public class PizzaDto {
    private UUID id;
    private String name;

    public PizzaDto() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
   }
}
~~~

Du côté de la ressource, on peut fournir une première implémentation :

~~~java

    @GET
  @Path("{id}")
  public PizzaDto getOnePizza(@PathParam("id") UUID id) {
    Pizza pizza = new Pizza();
    pizza.setId(id); // juste pour avoir le même id pour le test
    pizza.setName("4 fromages");
	  
	  return Pizza.toDto(pizza);
  }
~~~

Pour cette méthode, nous avons introduit la classe `Pizza`. Ce
JavaBean représente un ingrédient manipulé par la ressource.
Voici une implémentation pour cette classe :

~~~java
package fr.ulille.iut.pizzaland.beans;

import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
    private UUID id = UUID.randomUUID();
    private String name;

    public Pizza() {
    }

    public Pizza(String name) {
        this.name = name;
    }

    public Pizza(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static PizzaDto toDto(Pizza i) {
    	PizzaDto dto = new PizzaDto();
        dto.setId(i.getId());
        dto.setName(i.getName());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto pizzaDto) {
        Pizza pizza = new Pizza();
        pizza.setId(pizzaDto.getId());
        pizza.setName(pizzaDto.getName());

        return pizza;
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return "Ingredient [id=" + id + ", name=" + name + "]";
    }
}

~~~

Les méthodes `toDto()` et `fromDto()` permettrons de faire la conversion entre le Bean `Pizza` et le DTO qui représente ce qui sera transféré dans la requête/réponse HTTP.

Pour les différents beans que nous allons écrire générez également les méthodes `hashCode()` et `equals()` et `toString()`.

Le test devrait maintenant réussir :

~~~
$ mvn test
~~~

## Introduction de la persistence
Pour aller plus loin et mettre en place la création des pizzas il
va falloir introduire la persistence. Pour cela, nous allons utiliser
la librairie JDBI qui permet d'associer un modèle objet aux tables de
base de données.

Pour cela nous allons devoir implémenter le DAO (Data Access Object) `PizzaDao` :

~~~java
package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizza (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createTable();

    @SqlUpdate("DROP TABLE IF EXISTS pizza")
    void dropTable();

    @SqlUpdate("INSERT INTO pizza (id, name) VALUES (:id, :name)")
    void insert(@BindBean Pizza pizza);

    @SqlUpdate("DELETE FROM pizza WHERE id = :id")
    void remove(@Bind("id") UUID id);

    @SqlQuery("SELECT * FROM pizza WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM pizza")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();

    @SqlQuery("SELECT * FROM pizza WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(@Bind("id") UUID id);
}

~~~

JDBI fonctionne par annotations :
  - Les annotations `sqlUpdate` et `SqlQuery` correspondent à des
  requêtes SQL en modification ou non.
  - `@RegisterBeanMapper` permet d'associer une classe à un résultat
  (les champs de la table sont associés aux propriétés du bean).
  - `@Bind` permet d'associer un paramètre de méthode à un paramètre nommé dans la requête SQL.
  
Reprenons maintenant le code déjà écrit pour aller chercher les
ingrédients dans une base de données (nous utiliserons `Sqlite`).

### Les tests avec la base de données
Nous allons utiliser le DAO pour insérer des données dans la table
afin de réaliser nos tests. Nous utiliserons une base de données de
tests qui est définie via la classe `BDDFactory`.

Dans la classe `PizzaResourceTest`Les méthodes `setEnvUp` et `tearEnvDown` permettent de créer et
détruire la base de données entre chaque test.

~~~java
import fr.ulille.iut.pizzaland.dao.IngredientDao;
	
public class IngredientResourceTest extends JerseyTest {
  private IngredientDao dao;
	  
  @Override
  protected Application configure() {
     BDDFactory.setJdbiForTests();

     return new ApiV1();
  }
	
  @Before
  public void setEnvUp() {
    dao = BDDFactory.buildDao(IngredientDao.class);
    dao.createTable();
  }

  @After
  public void tearEnvDown() throws Exception {
     dao.dropTable();
  }
	
  @Test
  public void testGetExistingIngredient() {
    Ingredient ingredient = new Ingredient();
    ingredient.setName("Chorizo");
    dao.insert(ingredient);

    Response response = target("/ingredients").path(ingredient.getId().toString()).request(MediaType.APPLICATION_JSON).get();

    assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    Ingredient result = Ingredient.fromDto(response.readEntity(IngredientDto.class));
    assertEquals(ingredient, result);
}
~~~

### La ressource avec la base de données

~~~java
@Produces("application/json")
@Path("/pizza")
public class PizzaResource {
    private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
    
    private PizzaDao pizzas;
    
  @Context
  public UriInfo uriInfo;

  public PizzaResource() {
	  pizzas = BDDFactory.buildDao(PizzaDao.class);
	  pizzas.createPizzaTable();

  }

  @GET
  public List<PizzaDto> getAll() {
	  LOGGER.info("PizzaResource:getAll");
      List<PizzaDto> l = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
      LOGGER.info(l.toString());
      return l;

  }
  @GET
  @Path("{id}")
  @Produces({ "application/json", "application/xml" })
  public PizzaDto getOnePizza(@PathParam("id") UUID id) {
	  LOGGER.info("getOnePizza(" + id + ")");
      try {
          Pizza pizza = pizzas.findById(id);
          LOGGER.info(pizza.toString());
          return Pizza.toDto(pizza);
      } catch (Exception e) {
          throw new WebApplicationException(Response.Status.NOT_FOUND);
      }

  }
}
~~~

### Les tests fonctionnent avec la base de données
Nous pouvons maintenant vérifier que la base fonctionne avec la base
de données :

~~~
$ mvn test
	
Results :

Tests run: 2, Failures: 0, Errors: 0, Skipped: 0
~~~

## Complétons maintenant les différents tests
L'implémentation de la classe devrait fonctionner avec le test suivant
:

~~~java
  @Test
    public void testGetNotExistingPizza() {
      Response response = target("/pizza").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }
}

~~~

~~~
$ mvn test
	
Results :

Tests run: 3, Failures: 0, Errors: 0, Skipped: 0
~~~

### Implementation de la méthode POST
Il va falloir implémenter la méthode POST pour la création des
pizzas. Commençons par les différents tests : création, création
de deux pizzas identiques et création de pizzas sans nom.

~~~java
@Test
    public void testCreateIngredient() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("4 fromages");

        Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizza/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
    }

    @Test
    public void testCreateSameIngredient() {
        Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        dao.insert(pizza);

        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
        Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateIngredientWithoutName() {
        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizza").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

}
~~~

Nous utiliserons un DTO spécifique `PizzaCreateDto` dans la
mesure où nous n'aurons que le nom de l'ingrédient pour la création.

La classe [`jakarta.ws.rs.client.Entity<T>`](https://docs.oracle.com/javaee/7/api/jakarta/ws/rs/client/Entity.html) permet de définir le corps de
la requête POST et le type de données associée (ici `application/json`).

Nous devons également fournir une implémentation de
`PizzaCreateDto` pour pouvoir compiler notre code :

~~~java
package fr.ulille.iut.pizzaland.dto;

public class PizzaCreateDto {
	private String name;
	
	public PizzaCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}

}

~~~

Nous pouvons maintenant compiler notre code de test et constater que
ceux-ci échouent.

~~~
$ mvn test

Results :

Failed tests:   testCreateSamePizza(fr.ulille.iut.pizzaland.PizzaResourceTest): expected:<409> but was:<405>
	testCreatePizzaWithoutName(fr.ulille.iut.pizzaland.PizzaResourceTest): expected:<406> but was:<405>
	testCreatePizza(fr.ulille.iut.pizzaland.PizzaResourceTest): expected:<201> but was:<405>
	
Tests run: 6, Failures: 3, Errors: 0, Skipped: 0
~~~

Nous pouvons maintenant implémenter notre méthode POST dans la
	ressource :

~~~java
  @POST
  public Response createIngredient(PizzaCreateDto pizzaCreateDto) {
	  Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
      if (existing != null) {
          throw new WebApplicationException(Response.Status.CONFLICT);
      }

      try {
    	  Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
          pizzas.insert(pizza);
          PizzaDto pizzaDto = Pizza.toDto(pizza);

          URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

          return Response.created(uri).entity(pizzaDto).build();
      } catch (Exception e) {
          e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
      }

  }
~~~

Comme nous vérifions qu'il n'y a pas déjà une pizza avec le nom
fourni, nous devont ajouter une méthode `findbyName` à notre DAO

~~~java
@SqlQuery("SELECT * FROM pizza WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);
~~~

Nous avons également besoin de rajouter les méthodes de conversion
	pour ce DTO à notre bean `Pizza` :

~~~java
  public static PizzaCreateDto toCreateDto(Pizza pizza) {
    	PizzaCreateDto dto = new PizzaCreateDto();
        dto.setName(pizza.getName());
            
        return dto;
      }
    	
      public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
    	  Pizza pizza = new Pizza();
        pizza.setName(dto.getName());

        return pizza;
      }
~~~

Nous pouvons maintenant vérifier nos tests :

~~~
$ mvn test
	
Results :
	
Tests run: 6, Failures: 0, Errors: 0, Skipped: 0
~~~

Vous aurez peut-être un affichage d'exception liée au test de création
de doublon, toutefois le test est réussi puisqu'il a levé une
exception qui a été traduite par un code d'erreur HTTP 406.

### Implémentation de la méthode DELETE
Les tests liés à la méthode DELETE sont les suivants :

~~~java
  @Test
    public void testDeleteExistingPizza() {
    	Pizza pizza = new Pizza();
      pizza.setName("4 fromages");
      dao.insert(pizza);

      Response response = target("/pizza/").path(pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Pizza result = dao.findById(pizza.getId());
      assertEquals(result, null);
   }

   @Test
   public void testDeleteNotExistingPizza() {
     Response response = target("/pizza").path(UUID.randomUUID().toString()).request().delete();
     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }
~~~

Après avoir constaté que ces tests échouent, nous pouvons fournir une
implémentation pour la méthode DELETE :

~~~java
import jakarta.ws.rs.DELETE;
	
  @DELETE
  @Path("{id}")
  public Response deletePizza(@PathParam("id") UUID id) {
    if ( pizzas.findById(id) == null ) {
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

    pizzas.remove(id);

    return Response.status(Response.Status.ACCEPTED).build();
  }
~~~

Nous devons également implémenter la méthode remove dans
`IngredientDao` :

~~~java
   @SqlUpdate("DELETE FROM pizza WHERE id = :id")
	   void remove(@Bind("id") UUID i
~~~

Avec cette implémentation, nos tests réussissent.

### Implémentation de la méthode GET pour récupérer le nom de la pizza
Commençons par les tests correspondant à cette URI (GET
/pizza/{id}/name)

~~~java
   @Test
   public void testGetIngredientName() {
     Pizza pizza = new Pizza();
     pizza.setName("Chorizo");
     dao.insert(pizza);

     Response response = target("pizza").path(pizza.getId().toString()).path("name").request().get();

     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

     assertEquals("4 fromages", response.readEntity(String.class));
  }

  @Test
  public void testGetNotExistingIngredientName() {
    Response response = target("pizza").path(UUID.randomUUID().toString()).path("name").request().get();

    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
  
~~~

L'implémentation correspondant à ce test est simple :

~~~java
   @GET
  @Path("{id}/name")
  public String getPizzaName(@PathParam("id") UUID id) {
      Pizza pizza = pizzas.findById(id);

      if (pizza == null) {
          throw new WebApplicationException(Response.Status.NOT_FOUND);
      }

      return pizza.getName();
  }
~~~

### Implémentation d'une méthode de création avec des données de formulaire
La création d'une pizza pourrait également se faire via un
formulaire Web. Dans ce cas, le type de représentation sera
`application/x-www-form-urlencoded`. 

On peut déjà préparer un test pour cette méthode de création :

~~~java
   @Test
  public void testCreateWithForm() {
      Form form = new Form();
      form.param("name", "chorizo");

      Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
      Response response = target("pizza").request().post(formEntity);

      assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
      String location = response.getHeaderString("Location");
      String id = location.substring(location.lastIndexOf('/') + 1);
      Pizza result = dao.findById(UUID.fromString(id));

      assertNotNull(result);
  }
~~~

On peut maintenant fournir une implémentation pour cette méthode :

~~~java
    @POST
  @Consumes("application/x-www-form-urlencoded")
  public Response createPizza(@FormParam("name") String name) {
    Pizza existing = pizzas.findByName(name);
    if (existing != null) {
      throw new WebApplicationException(Response.Status.CONFLICT);
    }

    try {
    	Pizza pizza = new Pizza();
      pizza.setName(name);

      pizzas.insert(pizza);

      PizzaDto pizzaDto = Pizza.toDto(pizza);

      URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

      return Response.created(uri).entity(pizzaDto).build();
    } catch (Exception e) {
        e.printStackTrace();
        throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
    }
  }
~~~

# Créer une base de données de test
Nous avons maintenant implémenté et testé toutes les méthodes prévues
par notre API. Si nous voulons tester avec des clients, il serait bien
d'avoir quelques pizzas dans la base de données. Pour cela, nous
allons donner la possibilité de créer des pizzas au démarrage sur la base
d'une variable d'environnement : `PIZZAENV`.

Quand cette variable aura la valeur `withdb`, nous allons remplir la
base au démarrage avec le code suivant :

~~~java
package fr.ulille.iut.pizzaland;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.glassfish.jersey.server.ResourceConfig;

import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
    private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        packages("fr.ulille.iut.pizzaland");
        String environment = System.getenv("PIZZAENV");

        if ( environment != null && environment.equals("withdb") ) {
            LOGGER.info("Loading with database");
            try {
                FileReader reader = new FileReader( getClass().getClassLoader().getResource("pizza.json").getFile() );
                List<Pizza> pizzas = JsonbBuilder.create().fromJson(reader, new ArrayList<Pizza>(){}.getClass().getGenericSuperclass());

                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                pizzaDao.dropPizzaTable();
                pizzaDao.createPizzaTable();
                for ( Pizza pizza: pizzas) {
                        pizzaDao.insertPizza(pizza); 
                }
            } catch ( Exception ex ) {
                throw new IllegalStateException(ex);
            }
        } 
    }
}

~~~
    
Dans un terminal, nous pouvons maintenant fixer la variable
d'environnemnet et démarrer notre serveur REST au moyen de la
commande `mvn exec:java` :

~~~
$ export PIZZAENV=withdb
$ mvn exec:java
~~~	
Dans un autre terminal, nous pouvons utiliser `curl` pour tester nos
différentes méthodes :

~~~
$ curl -i localhost:8080/api/v1/pizza

HTTP/1.1 200 OK
Content-Type: application/json
Content-Length: 760


# Implémentation de la ressource Commande
Maintenant que vous avez une ressource `pizza` fonctionnelle, vous pouvez passer à l'implémentation de la ressource `Commandes`. Pour cette ressource, vous devrez d'abord définir l'API dans le fichier `commandes.md` (URI, méthodes, représentations). Ensuite, vous pourrez développer la ressource avec les tests associés.

Il est fortement recommandé d'adopter la même approche que pour `Ingredient` en développement progressivement les tests puis les fonctionnalitées associées.

